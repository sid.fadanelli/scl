#---------------------------------------
#  this 
#
#
#
#  fada 20
#---------------------------------------



import matplotlib as mpl
import numpy as np
import os 


import read_data 


class from_cef(object) : 
  
  def __init__(self,
               address): 
    """
    Creates the object to retrieve data from .cef files 
    
    Parameters :
     - address      [address]  where your data are (folder with .cef files)
    """
    
    self.address = os.path.normpath(address)
    self.meta = {}
    
  #---------------------------------------------------------------------------    
  def get_data(self,
               tar_file,
               toto_obj = None):
    """
    Loads the data from a cef. file (which is one scalar, one vector, one tensor etc.)
    Will fill up the given toto_obj or give back the data as donnee from read_data
    
    Parameters :
     - tar_file     [str] target file to read 
     - toto_obj     [None OR toto] toto object you want to fill, else returns donnee object 
     
    
    """    
    
    if tar_file[-4:] is '.cef' : tar_file = tar_file[:-4]
    
    cef_data = read_data.donnee(fichier = self.address+'/'+tar_file+'.cef')
    cef_data_names = sorted((set(cef_data.get_varnames()) - {'epoch', 'K001', 'K002'}))
    
    self.meta[tar_file] = cef_data._metadata
    self.meta[tar_file]['vars'] = ()
    
    tar_sc = self.meta[tar_file]['SC_ID'][0]
    for var in cef_data_names :
      self.meta[tar_file]['vars'] += (var+'-'+tar_sc,)

    if toto_obj is not None : 
      for var in cef_data_names :
        toto_obj.data[var+'-'+tar_sc] = cef_data.get_data(var).reshape(-1,1)
      toto_obj.meta['epoch'] = cef_data.donnee['epoch']
      #toto_obj.meta['fields'] = self.meta[tar_file]['fields']
    
    else : return cef_data



class from_cl(object) : 

  def __init__(self,
               address): 
    """
    Creates the object to put data from cl into .cef files 
    
    Parameters :
     - address      [address]  where your data are to be (folder with .cef files)
    """
    
    self.address = os.path.normpath(address)
    self.meta = {}

    
  #--------------------------------------------------------------------------
  def load_mec(self, probe=["1"], tar_vars=["r_gse"], datarate="srvy", lvl="l2"):
    """
    Load data from MEC instrument

    Parameters :
     - probe             [str]           Indices of selected probes
     - tar_vars          [str]           Variables to load 
     - datarate          [str]           Data acquisition rate
     - lvl               [str]           Level of data
    """

    mecvarsDict =  ["dipole_tilt",      "gmst",                 "mlat",             "mlt",                  \
                        "l_dipole",         "quat_eci_to_bcs",      "quat_eci_to_dbcs", "quat_eci_to_dmpa",     \
                        "quat_eci_to_smpa", "quat_eci_to_dsl",      "quat_eci_to_ssl",  "L_vec",                \
                        "Z_vec",            "P_vec",                "L_phase",          "Z_phase",              \
                        "P_phase",          "kp",                   "dst",              "earth_eclipse_flag",   \
                        "moon_eclipse_flag","r_eci",                "v_eci",            "r_gsm",                \
                        "v_gsm",            "quat_eci_to_gsm",      "r_geo",            "v_geo",                \
                        "quat_eci_to_geo",  "r_sm",                 "v_sm",             "quat_eci_to_sm",       \
                        "r_gse",            "v_gse",                "quat_eci_to_gse",  "r_gse2000",            \
                        "v_gse2000",        "quat_eci_to_gse2000",  "geod_lat",         "geod_lon",             \
                        "geod_height",      "r_sun_de421_eci",      "r_moon_de421_eci", "fieldline_type",       \
                        "bsc_gsm",          "loss_cone_angle_s",    "loss_cone_angle_n","pfs_geod_latlon",      \
                        "pfn_geod_latlon",  "pfs_gsm",              "bfs_gsm",          "pfn_gsm",              \
                        "bfn_gsm",          "pmin_gsm",             "bmin_gsm"]

  
  #--------------------------------------------------------------------------
  def load_fgm(self, probe=["1"], tar_vars=["r_gse"], datarate="srvy", lvl="l2"):
    """
    Load data from FGM instrument

    Parameters :
     - probe             [str]           Indices of selected probes
     - tar_vars          [str]           Variables to load 
     - datarate          [str]           Data acquisition rate
     - lvl               [str]           Level of data
    """    
    
  #--------------------------------------------------------------------------
  def load_scm(self, probe=["1"], tar_vars=["r_gse"], datarate="srvy", lvl="l2"):
    """
    Load data from SCM instrument

    Parameters :
     - probe             [str]           Indices of selected probes
     - tar_vars          [str]           Variables to load 
     - datarate          [str]           Data acquisition rate
     - lvl               [str]           Level of data
    """

  #--------------------------------------------------------------------------
  def load_edp(self, probe=["1"], tar_vars=["r_gse"], datarate="srvy", lvl="l2"):
    """
    Load data from EDP instrument

    Parameters :
     - probe             [str]           Indices of selected probes
     - tar_vars          [str]           Variables to load 
     - datarate          [str]           Data acquisition rate
     - lvl               [str]           Level of data
    """

  #--------------------------------------------------------------------------
  def load_dsp(self, probe=["1"], tar_vars=["r_gse"], datarate="srvy", lvl="l2"):
    """
    Load data from DSP instrument

    Parameters :
     - probe             [str]           Indices of selected probes
     - tar_vars          [str]           Variables to load 
     - datarate          [str]           Data acquisition rate
     - lvl               [str]           Level of data
    """

  #--------------------------------------------------------------------------
  def load_fpi(self, probe=["1"], tar_vars=["r_gse"], datarate="srvy", lvl="l2"):
    """
    Load data from FPI instrument

    Parameters :
     - probe             [str]           Indices of selected probes
     - tar_vars          [str]           Variables to load 
     - datarate          [str]           Data acquisition rate
     - lvl               [str]           Level of data
    """



#class from_spedas(object) :  #note that in this case it is necessary to install apposite packages
  
  
  

#class from_npsave(object) : #
  
  #def __init__(self,
	       
  
