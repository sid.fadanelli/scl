#! /bin/env python
# coding: utf8

import ceflib
from datetime import datetime
from datetime import timedelta
import numpy as np
import os
import matplotlib
import matplotlib.pyplot as plt
import pickle
import string as st
##from numpy import fft as fft

import writecef

class donnee(object) :
  
  def __init__(self, fichier, virgin=False) :
    """
    Donnee object : open data from cef file
    This object is useful to manage timeseries with implemented calculation on timedeltas
    get metadata from ceffile and store it in self._metadata
    to load data use self._get_data() (THIS MUST BE IMPROVED)
    all data are stored in self.donnee
    """
    self.donnee = {}
    self._varnames = np.array([], dtype='|S15') 
    ceflib.verbosity(0)
    
    if not virgin :
      self._fichier = fichier
      self._get_time()
      
      
      ceflib.read_metadata(self._fichier)
      self._metanames = np.copy(ceflib.metanames())
      self._metadata = {}
      self._varnames = np.copy(ceflib.varnames())
      for name in self._metanames : self._metadata[name] = np.copy(ceflib.meta(name))
      ceflib.close()
  
  def _get_time(self) :
    ceflib.read(self._fichier)
    self.donnee['epoch'] = np.copy(np.array([datetime.utcfromtimestamp(ceflib.milli_to_timestamp(i)) for i in ceflib.var('epoch')]))
    ceflib.close()
  
  def get_time(self) :
    if 'epoch' not in self.donnee : self._get_time()
    #self.donnee['epoch'] = self.donnee['epoch'][2:-2]       ### WARNING this is just to fix the clExport bug 
    return self.donnee['epoch']
  
  def get_varnames(self) :
    return self._varnames
    
  def _get_data(self, var = 'all') :
    #self.donnee = {}
    ceflib.read(self._fichier)
    if var == 'all' :
      for nom in self._varnames:
        if nom not in ['epoch', 'K001', 'K002'] : self.donnee[nom] = np.copy(ceflib.var(nom))
    else : self.donnee[var] = np.copy(ceflib.var(var))
    #self.donnee[var] = self.donnee[var][2:-2]         ### WARNING this is just to fix the clExport bug 
    ceflib.close()
  
  def get_data(self, var) :
    self._load_var(var)
    return self.donnee[var]
  
  def get_data_as_field(self, varz, t1 = None, t2 = None, checkorder = False, withtime = False) :
    """
    input as many field as wanted
    return a numpy array of len(varz) times len(data) dimension, columns are sorted with varz names
    t1 and t2 (optionnal) : if only a particular interval is required, input as time
    WARNING : = used in this function instead of np.copy, this may lead to some issues
    """
    for var in varz : self._load_var(var)
    out = np.zeros([len(self.donnee['epoch']), len(varz)])
    for i, var in enumerate(sorted(varz)) : out[:,i] = self.donnee[var]
    if withtime : out = np.column_stack((self.donnee['epoch'], out))
    if checkorder : 
      print('columns ordered as ')
      print(sorted(varz))
    
    if t1 : t1 = abs(self.donnee['epoch'] - t1).argmin()
    if t2 : t2 = abs(self.donnee['epoch'] - t2).argmin()
    return out[t1:t2, :]
  
  def get_std_as_field(self, varz, t1 = None, t2 = None) :
    """
    use self.get_data_as_field() and mean it
    return a numpy array of len(varz) dimension
    """
    out2 = np.zeros(len(varz))
    out = self.get_data_as_field(varz, t1, t2)
    for i in range(len(varz)) : out2[i] = out[:,i].std()
    return out2
  
  def get_std(self, t1, t2, var) :
    self._load_var(var)
    i1 = abs(self.donnee['epoch'] - t1).argmin()
    i2 = abs(self.donnee['epoch'] - t2).argmin()
    return self.donnee[var][i1:i2].std()
  
  def get_mean_as_field(self, varz, t1 = None, t2 = None) :
    """
    use self.get_data_as_field() and mean it
    return a numpy array of len(varz) dimension
    """
    out2 = np.zeros(len(varz))
    out = self.get_data_as_field(varz, t1, t2)
    for i in range(len(varz)) : out2[i] = out[:,i].mean()
    return out2
  
  def get_mean(self, t1, t2, var) :
    self._load_var(var)
    i1 = abs(self.donnee['epoch'] - t1).argmin()
    i2 = abs(self.donnee['epoch'] - t2).argmin()
    return self.donnee[var][i1:i2].mean()
  
  def getIndex(self, t = None):
    if t : return abs(self.donnee['epoch'] - t).argmin()
    else : return None
  
  def get_projected(self, varz, matrice, new_coord = ['l', 'm', 'n']) :
    """
    !!! Only works with 3D array at the moment
    user must provide a transport matrix, with L,M,N in each colums of the matrix, so that :
    new_vect = old_vect([x,y,z]) * matrix, or new_vect = inv(matrix) * old_vect[[x], [y], [z]]
    """
    field = self.get_data_as_field(varz)
    f = np.array([vect * matrice for vect in field])[:,0]
    self.donnee[new_coord[0]] = f[:,0]
    self.donnee[new_coord[1]] = f[:,1]
    self.donnee[new_coord[2]] = f[:,2]
  
  def _load_var(self, var) :
    if var not in self.donnee :
      ceflib.read(self._fichier)
      self.donnee[var] = np.copy(ceflib.var(var))
      ceflib.close()
  
  
  def addField(self, field, abscisse = None, name = 'New_interpolated_field') :
    """
    Add new field in the donnee object, use old abscisse to interpolate on current absisse
    If no abscisse is given... do something? 
    """
    def interp(xnew, xold, yold) :
      def tofloat(d) :
        ref = datetime(2001,1,1) #Timedelta Calculé à partir de 2001!!
        return (d-ref).total_seconds()
      tofloat = np.vectorize(tofloat)
      ynew  = np.interp(tofloat(xnew),tofloat(xold) ,yold)
      return ynew

    self.donnee[name] = interp(self.donnee['epoch'], abscisse, field)
    return self.donnee[name]
    
  
  def get_interval(self, var, t1 = None, t2 = None, include = False) :
    """
    get part of the data set, between start time t1 and stop time t2
    include : include last element, python drop it by default (default is False)
    """
    self._load_var(var)
    if t1 : t1 = abs(self.donnee['epoch'] - t1).argmin()
    if t2 : 
      t2 = abs(self.donnee['epoch'] - t2).argmin()
      return self.donnee[var][t1:t2 + int(include)]
    else : return self.donnee[var][t1:t2]
        
  def get_metadata(self, name = None) :
    if name in self._metadata : return self._metadata[name]
    else : return ''
  
  def get_metanames(self) :
    return [name for name in self._metadata]

  #def plot(self, var, show = True, newfig = False, color=None) :
    #if not isinstance(var, list) : var = [var,]
    #if newfig : f = plt.figure()
    #for v in var : 
      #print 'plotting', v
      #self._load_var(v)
      #plt.plot_date(self.donnee['epoch'], self.donnee[v], '-', label = v, color=color)
    
    #plt.legend()
    #if show : plt.show()
  
  def plot(self,           #plotting routine
      var,        #variables to plot
      show = True,      #show off?
      newfig = False,      #new figure?
      color=None,      #color
      t1=None,      #begin time
      t2=None,      #end time
      legend = None,      #legend, one label for each component in the var list ..
      yscale = 'linear'):    #set 'linear' or 'log' ... I don't need to explain) :
    
    if not isinstance(var, list) : var = [var,]
    if newfig : f = plt.figure()
    
    x_ax = self.new_epoch(t1=t1,t2=t2)
    
    for v in var : 
      print ('plotting '+v )
      
      y_ax, ww = my_reshape(self.new_field(var=v,t1=t1,t2=t2))
      var_number = np.shape(y_ax)[1]
      
      lineObjects = plt.plot_date(x_ax, y_ax, '-' , color=color)
      if legend != None : plt.legend(iter(lineObjects), legend)
    
    
    if show : 
       plt.show()
       
  
  def new_epoch(self,       #creates a new 'epoch' object, compatible with the scalar given by the 'new_scalar' routine 
      half_width=0,     #half-width of the scalar-averaging window
      step=1,     #distance between two consecutive local averages
      t1=None,     #begin time
      t2=None):    #end time
    
    if t1 : t1 = abs(self.donnee['epoch'] - t1).argmin()
    else: t1 = 0
    if t2 : t2 = abs(self.donnee['epoch'] - t2).argmin() +1
    
    if half_width != 0:
      original_length = (self.donnee['epoch']).__len__() 
      old_length = (self.donnee['epoch'][t1:t2]).__len__() 
      record = np.zeros(original_length, dtype=bool)
      #print range(t1+half_width, t1+old_length-half_width, step)
      for i in range(t1+half_width, t1+old_length-half_width, step):
        record[i] = True
      return np.array(self.donnee['epoch'])[record]
    else: 
      return self.donnee['epoch'][t1:t2]
  
  def new_scalar(self,       #creates a new scalar object, via moving-window average of an old scalar, applied between two specified times 
      var,      #the variable to be averaged
      half_width=0,     #half-width of the averaging window
      step=1,     #distance between two consecutive local averages
      weights=None,    #weights of the average inside the moving window
      t1=None,    #begin time
      t2=None):    #end time
    
    if t1 : t1 = abs(self.donnee['epoch'] - t1).argmin()
    else: t1 = 0
    if t2 : t2 = abs(self.donnee['epoch'] - t2).argmin() +1
    
    old_array = self.donnee[var]
    
    if half_width != 0:
      old_length = len(self.donnee['epoch'][t1:t2])
      new_length = (old_length - 2*half_width + step - 1) // step
      new_array = np.zeros(new_length, dtype=float)
      
      j = 0 
      for i in range(t1+half_width, t1+old_length-half_width, step):
        new_array[j] = np.average(old_array[i-half_width:i+half_width],weights=weights)
        j += 1
      return new_array
    else : 
      return old_array[t1:t2]
    
  def new_field(self,       #creates a new field object, via moving-window average of an old scalar, applied between two specified times 
      var,      #the variable to be averaged
      half_width=0,     #half-width of the averaging window
      step=1,     #distance between two consecutive local averages
      weights=None,    #weights of the average inside the moving window
      t1=None,    #begin time
      t2=None):    #end time
    
    if t1 : t1 = abs(self.donnee['epoch'] - t1).argmin()
    else: t1 = 0
    if t2 : t2 = abs(self.donnee['epoch'] - t2).argmin() +1
    
    
    old_array, shape = my_reshape(self.donnee[var])
    shape = list(shape)
    length = len(self.donnee['epoch'][t1:t2])
    
    if half_width == 0 and step == 1 :
      shape[0] = length
      return np.reshape(old_array[t1:t2,:], tuple(shape))
    else : 
      new_length = (length - 2*half_width + step - 1) // step
      new_width = np.shape(old_array)[1]
      
      new_array = np.zeros((new_length,new_width), dtype=float)
      
      j = 0 
      for i in range(t1+half_width, t1+length-half_width, step):
        new_array[j,:] = np.average(old_array[i-half_width:i+half_width,:],weights=weights,axis=0)
        j += 1
      
      shape[0] = new_length
      return np.reshape(new_array,tuple(shape))
  
  #self.AbscSize = {} I would put these in the donnee (as properties), maybe 
  
  def timestep(self): 
    timestep = tofloat(self.donnee['epoch'][-1]) - tofloat(self.donnee['epoch'][0])
    return timestep / len(self.donnee['epoch']) 
    print(str(timestep)+' '+str(len(self.donnee['epoch'])))
  

def time_interpolation(x1, x2, y2) :
  """
  Time interpolation for 2D data :
  x1 = reference abscisse (in datetime objects)
  x2, y2 = abscisse and ordinate of data to interpolate
  return y3 = y2 on x1
  """
  
  if y2.dtype != 'float' : y2 = y2.astype('float')
  
  y3  = np.interp(tofloat(x1),tofloat(x2),y2)
  return y3
