#! /bin/env python
# coding: utf8



#-----------------------------------------------

#these should go into the scd_toto
from read_data import donnee
#from read_data import list_data
from read_data import time_interpolation
from writecef import cefwrite

from scl_main import toto

#-----------------------------------------------

#these should get into the scd_loaders i.e. from_cef, from_spedas, from_npsave 
from clcom import tt_all
#from clExport import clExport #you cannot activate it on python3 as of now, because there is no Tkinter installed

from scl_load import from_cef      #refer to read_data
#from scd_load import from_spedas   #refer to fibo_MMS
#from scd_load import from_npsave   #refer to read_data

