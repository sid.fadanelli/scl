#! /bin/env python
# coding: utf8



import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import os 
import pandas
import pickle
import scipy.interpolate as scint
import string as st
##from numpy import fft as fft

import ceflib
import writecef

from datetime import datetime
from datetime import timedelta


class toto(object) : 

  #-----------------------------------------------------------------------------------
  def __init__(self,
      toto_name):
    
    self.toto_name = toto_name
    
    self.data = {}
    self.meta = {}   
    self.stat = {}
    
  #-----------------------------------------------------------------------------------
  def get_data(self,
      tar_var): 
    """
    Gives you the data array, overrunning toto privileged data storage ...
    
    Parameters: 
     - tar_var [str OR np.ndarray] name in list(toto.data) OR (nt,...) array
    """
    
    if   isinstance(tar_var, str) : return self.data[tar_var]
    elif isinstance(tar_var, np.ndarray) : return tar_var
    else : 
      print('??WTW??')
      return None


  #-----------------------------------------------------------------------------------
  def set_field(self,
      comp_list): 
    """
    Builds up the target field as np-lin field from a list of variables, which also deletes  
    
    Parameters:
     - comp_list   [list of str OR np.ndarray] name in list(toto.data) OR (nt,...) array
    
    """
    
    field = np.block([self.get_data(c) for c in comp_list])
    
    for c in comp_list : del( self.data[c] )
    
    return field

  #-----------------------------------------------------------------------------------
  def set_shape(self,
      tar_var,
      tar_shape): 
    """
    Gives you the tar_var array reshaped 
    
    Parameters:
     - tar_var      [str OR np.ndarray] name in list(toto.data) OR (nt,...) array
     - tar_shape    ['np-lin' OR 'np-ten'] 
    
    """
  
    tar_var = self.get_data(tar_var)
    
    # this is the standard way that every function accepts 
    if tar_shape == 'np-lin' : 
      dims = 1
      for i in range(1,len(tar_var.shape)): 
        dims = dims*tar_var.shape[i]
        
      return np.reshape(tar_var,(tar_var.shape[0],dims))
    
    # this is what you might prefer in order to write your functions 
    elif tar_shape == 'np-ten' : 
      if tar_var.shape[1] == 9 : 
        shape = (tar_var.shape[0],3,3)
      elif tar_var.shape[1] == 3 : 
        shape = (tar_var.shape[0],3)
      elif tar_var.shape[1] == 1 : 
        shape = (tar_var.shape[0],)
      else: print('What kind of variable are you dealing with ???')
      
      return np.reshape(tar_var,shape)
    
    else : print ('choose among one of the possible shapes: np-lin OR np-ten')

  #-----------------------------------------------------------------------------------
  def make_t_int(self,
      tar_var,
      tar_epoch,
      int_kwargs = {'kind':'cubic', 'fill_value':'extrapolate'}): 
    """
    Gives you the tar_var array interpolated over the tar_epoch time
    
    Parameters:
     - tar_var    [str OR np.ndarray] name in list(toto.data) OR (nt,...) array
     - tar_epoch  [epoch_array] 
     - int_kwargs [kwargs of scipy.interpolate.interp1d]
    
    """
  
    def epoch_to_seconds(d) :
      ref = self.meta['epoch'][0] 
      return (d-ref).total_seconds()
    epoch_to_seconds = np.vectorize(epoch_to_seconds)
    
    tar_var = self.set_shape(tar_var,'np-lin')
    new_var = np.empty((tar_epoch.shape[0],)+(tar_var.shape[1],))
    
    old_epoch = epoch_to_seconds(self.meta['epoch'])
    new_epoch = epoch_to_seconds(tar_epoch)
    
    for col in range(tar_var.shape[1]) : 
      #new_var[:,col] = np.interp(tofloat(tar_epoch),tofloat(self.meta['epoch']),tar_var[:,col])
      interpolator = scint.interp1d(old_epoch,tar_var[:,col],**int_kwargs)
      new_var[:,col] = interpolator(new_epoch)
    
    return new_var
  
  #-----------------------------------------------------------------------------------
  def make_t_ave(self,
      tar_var,
      hawi=0, 
      step=1,    
      weights=None,  
      t1=None, 
      t2=None):   
    """
    Gives you the tar_var array averaged by moving window 
    
    Parameters:
     - tar_var         [str OR np.ndarray] name in list(toto.data) OR (nt,...) array
     - hawi=0          [int] half-width of the averaging window
     - step=1          [int] distance between two consecutive local averages
     - weights=None    weights of the average inside the moving window
     - t1=None         begin time
     - t2=None         end time    
    """
  
    if t1 is not None : t1 = abs(self.meta['epoch'] - t1).argmin()
    else: t1 = 0
    if t2 is not None : t2 = abs(self.meta['epoch'] - t2).argmin() +1
    
    old_array = self.set_shape(tar_var,'np-lin')
    length = len(self.meta['epoch'][t1:t2])
  
    if hawi == 0 and step == 1 :
      return old_array[t1:t2,:]

    else : 
      new_length = (length - 2*hawi + step - 1) // step
      new_width = np.shape(old_array)[1]
      
      new_array = np.zeros((new_length,new_width), dtype=float)
      j = 0 
      for i in range(t1+hawi, t1+length-hawi, step):
        new_array[j,:] = np.average(old_array[i-hawi:i+hawi,:],weights=weights,axis=0)
        j += 1
      
      return new_array

  #-----------------------------------------------------------------------------------/
  def make_t_int_epoch(self,
      points,  
      t1=None,
      t2=None): 
    """
    Gives you the epoch relative to a tar_var array averaged by moving window 
    
    Parameters:
     - points       [int] number of points to be set
     - t1=None      [epoch] begin time
     - t2=None      [epoch] end time
    """
  
    if t1 is not None : t1 = abs(self.meta['epoch'] - t1).argmin()
    else : t1 = self.meta['epoch'][0]
    if t2 is not None : t2 = abs(self.meta['epoch'] - t2).argmin() +1
    else : t2 = self.meta['epoch'][-1]
    
    return pandas.date_range(t1,t2,points).to_pydatetime()

  #-----------------------------------------------------------------------------------
  def make_t_ave_epoch(self,
      hawi=0,     
      step=1,  
      t1=None,
      t2=None): 
    """
    Gives you the epoch relative to a tar_var array averaged by moving window 
    
    Parameters:
     - hawi=0     [int] half-width of the scalar-averaging window
     - step=1     [int] distance between two consecutive local averages
     - t1=None    begin time
     - t2=None    end time
    """
  
    if t1 is not None : t1 = abs(self.meta['epoch'] - t1).argmin()
    else : t1 = 0
    if t2 is not None : t2 = abs(self.meta['epoch'] - t2).argmin() +1
    
    if hawi != 0:
      original_length = (self.meta['epoch']).__len__() 
      old_length = (self.meta['epoch'][t1:t2]).__len__() 
      record = np.zeros(original_length, dtype=bool)
      #print range(t1+hawi, t1+old_length-hawi, step)
      for i in range(t1+hawi, t1+old_length-hawi, step):
        record[i] = True
      return np.array(self.meta['epoch'])[record]

    else: 
      return self.meta['epoch'][t1:t2]


  #-----------------------------------------------------------------------------------
  def calc_recp(self,
      tar_var): 
    """
    Calculates the vector reciprocal for four-satellite configurations 
    
    Parameters: 
     - tar_var [str OR np.ndarray] name in list(toto.data) OR (nt,...) array
    """
    
    K = {}
    absc_size = len(self.meta['epoch'])
    
    for s in range(1,5):
      for ss in range(1,5):
        K[str(s)+'D'+str(ss)] = np.add(
          self.get_data(tar_var+'_SC'+str(s)),-self.get_data(tar_var+'_SC'+str(ss)))

    sat_perm = [1,2,3,4,1,2,3]
    for s in range(1,5):
      K['inv_SC'+str(s)] = np.zeros([absc_size,3], dtype=float)

      for k in range(absc_size):
        K['inv_SC'+str(s)][k,:] = np.cross(
          K[str(sat_perm[s])+'D'+str(sat_perm[s+1])][k,:],
          K[str(sat_perm[s])+'D'+str(sat_perm[s+2])][k,:])
        K['inv_SC'+str(s)][k,:] = np.divide(
          K['inv_SC'+str(s)][k,:],np.dot(
          K[str(sat_perm[s-1])+'D'+str(sat_perm[s])][k,:],
          K['inv_SC'+str(s)][k,:]))
    
    return K['inv_SC1'], K['inv_SC2'], K['inv_SC3'], K['inv_SC4']
    
  #-----------------------------------------------------------------------------------
  def calc_grad(self,
      tar_var_list,
      rec_pos_list): 
    """
    Calculates the vector reciprocal for four-satellite configurations 
    
    Parameters: 
     - tar_var  list of four toto_var
     - rec_pos  list of four vector reciprocals for position
    """

    absc_size = len(self.meta['epoch'])

    #determine the original shape and that of the tensor  
    sha0 = np.insert(self.get_data(tar_var_list[0]).shape, 1, 3) 
    sha1 = np.shape(self.set_shape(tar_var_list[0],'np-lin'))
    tens = np.zeros([absc_size,3,sha1[1]], dtype=float)
  
    for r in range(4):
      vect = self.set_shape(tar_var_list[r],'np-lin')
      for k in range(absc_size):
        tens[k,:,:] += np.tensordot(self.get_data(rec_pos_list[r])[k,:],vect[k,:],axes=0)

    return np.reshape(tens, sha0) 

  #-----------------------------------------------------------------------------------
  def draw_canvas(self,
        tar_labs,  
        tar_dims):
    """ 
    Prepares canvas for drawing
    
    Parameters :
      - tar_labs    [list of list of str] matrix of labels
      - tar_dims    [6*[float] OR 'line' OR 'cont_h' OR 'cont_v'] dims of figs and spaces within
    
    Returns :
      - multi_sub   [blank figure] 
    
    """ 
  
    y_fig_num, x_fig_num = np.shape(tar_labs)
  
    if tar_dims is 'line'   : tar_dims = [7.2,3.0,0.8,0.6,0.6,0.6]
    if tar_dims is 'cont_h' : tar_dims = [5.2,5.2,0.8,0.6,0.6,0.6]
    if tar_dims is 'cont_v' : tar_dims = [6.0,6.0,0.6,0.6,0.6,0.6]
  
    dim_x = tar_dims[0]*x_fig_num + tar_dims[2]*(x_fig_num-1) + 2*tar_dims[4]
    dim_y = tar_dims[1]*y_fig_num + tar_dims[3]*(y_fig_num-1) + 2*tar_dims[5]
  
    multi_fig = plt.figure(figsize=(dim_x,dim_y))
    multi_fig.patch.set_facecolor('white')
  
    for ii in range(y_fig_num):
      for jj in range(x_fig_num):
        pos_x = (tar_dims[4] + (tar_dims[0] + tar_dims[2]) * jj)/dim_x
        pos_y = (tar_dims[5] + (tar_dims[1] + tar_dims[3]) * (y_fig_num-1-ii) )/dim_y
  
        multi_fig.add_axes([pos_x,pos_y,tar_dims[0]/dim_x,tar_dims[1]/dim_y])
        plt.ylabel(tar_labs[ii][jj])
  
    multi_sub = multi_fig.get_axes()
    #for ax in multi_sub : 
    #  ax.xaxis.set_tick_params(labelsize=tar_char)#  axis='both', which='major', )
    #  ax.yaxis.set_tick_params(labelsize=tar_char)
    
    return multi_sub

  #-----------------------------------------------------------------------------------  
  def draw_heatmap(self,   
        multi_sub,
        tar_vars_a,
        tar_vars_b,
        plot_range_a,
        plot_range_b,
        bin_number = [100,100],
        args_fig = {'cmap':None,'norm':None,'alpha':0.6},
        args_bar = {'aspect':30},
        ticks_x = None,
        ticks_y = None,
        ticks_b = None,
        labels_x = None,
        labels_y = None,
        labels_b = None): 
    """ 
    Draws heatmap of correlations between variables
    
    Parameters :
      - multi_sub         [list of (sub)plot] in you are drawing
      - tar_vars_a        [list of fibo.data] you want to plot
      - tar_vars_b        [list of fibo.data] you want to plot
      - plot_range_a      [int,int] range of a values in the plot
      - plot_range_b      [int,int] range of b values in the plot
      - bin_number        [int,int] number of bins in x and y
      - args_fig          [pcolor-kwargs]
      - args_bar          [None OR colorbar kwargs]
      - ticks_x           [None OR list of set_tick_params-kwargs]
      - ticks_y           [None OR list of set_tick_params-kwargs]
      - ticks_b           [None OR list of set_tick_params-kwargs]
      - labels_x          [None OR list of set_label-kwargs]
      - labels_y          [None OR list of set_label-kwargs]
      - labels_b          [None OR list of set_label-kwargs]
          
    """
  
  
    if ticks_x is None : ticks_x = len(multi_sub)*[{'labelsize':9}]
    if ticks_y is None : ticks_y = len(multi_sub)*[{'labelsize':9}]
    if ticks_b is None : ticks_b = len(multi_sub)*[{'labelsize':9}]
    if labels_x is None : labels_x = len(multi_sub)*[{'text':''}]
    if labels_y is None : labels_y = len(multi_sub)*[{'text':''}]
    if labels_b is None : labels_b = len(multi_sub)*[{'text':''}]
    
    bins = {}
    bins[0] = np.linspace(plot_range_a[0],plot_range_a[1],bin_number[0])
    bins[1] = np.linspace(plot_range_b[0],plot_range_b[1],bin_number[1])
    
    for plt_num, plts in enumerate(multi_sub):
      plts.xaxis.set_tick_params(**ticks_x[plt_num])
      plts.yaxis.set_tick_params(**ticks_y[plt_num])
      plts.set_xlabel('',**labels_x[plt_num])
      plts.set_ylabel('',**labels_y[plt_num])
  
      allpoints = [self.data[tar_vars_a[plt_num]] ,self.data[tar_vars_b[plt_num]] ]
      allpoints =   np.transpose(np.vstack(allpoints))
  
      hist, edges = np.histogramdd(allpoints,bins=bins)
      cpts = plts.pcolor(edges[1],edges[0],hist, **args_fig) #cmap=cmap,alpha=alpha,norm=norm)
  
      plts.grid(True,which='both')
      if args_bar is not None : 
        cbar = plt.colorbar(cpts,ax=plts,**args_bar)
        cbar.ax.xaxis.set_tick_params(**ticks_b[plt_num])
        cbar.ax.yaxis.set_tick_params(**ticks_b[plt_num])
        cbar.set_label('',**labels_b[plt_num])





'''
  
  def get_std_as_field(self, varz, t1 = None, t2 = None) :
    """
    use self.get_data_as_field() and mean it
    return a numpy array of len(varz) dimension
    """
    out2 = np.zeros(len(varz))
    out = self.get_data_as_field(varz, t1, t2)
    for i in range(len(varz)) : out2[i] = out[:,i].std()
    return out2
  
  def get_std(self, t1, t2, var) :
    self._load_var(var)
    i1 = abs(self.meta['epoch'] - t1).argmin()
    i2 = abs(self.meta['epoch'] - t2).argmin()
    return self.data[var][i1:i2].std()
  
  def get_mean_as_field(self, varz, t1 = None, t2 = None) :
    """
    use self.get_data_as_field() and mean it
    return a numpy array of len(varz) dimension
    """
    out2 = np.zeros(len(varz))
    out = self.get_data_as_field(varz, t1, t2)
    for i in range(len(varz)) : out2[i] = out[:,i].mean()
    return out2
  
  def get_mean(self, t1, t2, var) :
    self._load_var(var)
    i1 = abs(self.meta['epoch'] - t1).argmin()
    i2 = abs(self.meta['epoch'] - t2).argmin()
    return self.data[var][i1:i2].mean()
  
  def getIndex(self, t = None):
    if t : return abs(self.meta['epoch'] - t).argmin()
    else : return None
  
  def get_projected(self, varz, matrice, new_coord = ['l', 'm', 'n']) :
    """
    !!! Only works with 3D array at the moment
    user must provide a transport matrix, with L,M,N in each colums of the matrix, so that :
    new_vect = old_vect([x,y,z]) * matrix, or new_vect = inv(matrix) * old_vect[[x], [y], [z]]
    """
    field = self.get_data_as_field(varz)
    f = np.array([vect * matrice for vect in field])[:,0]
    self.data[new_coord[0]] = f[:,0]
    self.data[new_coord[1]] = f[:,1]
    self.data[new_coord[2]] = f[:,2]
  
  def _load_var(self, var) :
    if var not in self.data :
      ceflib.read(self._fichier)
      self.data[var] = np.copy(ceflib.var(var))
      ceflib.close()
  
  
  def addField(self, field, abscisse = None, name = 'New_interpolated_field') :
    """
    Add new field in the donnee object, use old abscisse to interpolate on current absisse
    If no abscisse is given... do something? 
    """
    def interp(xnew, xold, yold) :
      def tofloat(d) :
        ref = datetime(2001,1,1) #Timedelta Calculé à partir de 2001!!
        return (d-ref).total_seconds()
      tofloat = np.vectorize(tofloat)
      ynew  = np.interp(tofloat(xnew),tofloat(xold) ,yold)
      return ynew

    self.data[name] = interp(self.meta['epoch'], abscisse, field)
    return self.data[name]
    
  
  def get_interval(self, var, t1 = None, t2 = None, include = False) :
    """
    get part of the data set, between start time t1 and stop time t2
    include : include last element, python drop it by default (default is False)
    """
    self._load_var(var)
    if t1 : t1 = abs(self.meta['epoch'] - t1).argmin()
    if t2 : 
      t2 = abs(self.meta['epoch'] - t2).argmin()
      return self.data[var][t1:t2 + int(include)]
    else : return self.data[var][t1:t2]
        
  def get_metadata(self, name = None) :
    if name in self._metadata : return self._metadata[name]
    else : return ''
  
  def get_metanames(self) :
    return [name for name in self._metadata]

  #def plot(self, var, show = True, newfig = False, color=None) :
    #if not isinstance(var, list) : var = [var,]
    #if newfig : f = plt.figure()
    #for v in var : 
      #print 'plotting', v
      #self._load_var(v)
      #plt.plot_date(self.meta['epoch'], self.data[v], '-', label = v, color=color)
    
    #plt.legend()
    #if show : plt.show()
  
  def plot(self,           #plotting routine
      var,        #variables to plot
      show = True,      #show off?
      newfig = False,      #new figure?
      color=None,      #color
      t1=None,      #begin time
      t2=None,      #end time
      legend = None,      #legend, one label for each component in the var list ..
      yscale = 'linear'):    #set 'linear' or 'log' ... I don't need to explain) :
    
    if not isinstance(var, list) : var = [var,]
    if newfig : f = plt.figure()
    
    x_ax = self.new_epoch(t1=t1,t2=t2)
    
    for v in var : 
      print 'plotting', v
      
      y_ax, ww = my_reshape(self.new_field(var=v,t1=t1,t2=t2))
      var_number = np.shape(y_ax)[1]
      
      lineObjects = plt.plot_date(x_ax, y_ax, '-' , color=color)
      if legend != None : plt.legend(iter(lineObjects), legend)
    
    
    if show : 
       plt.show()
       


###allows easier use of donnee objects
class multi(object) :
	def __init__(self): 			#create the wonderful object (step 0 always)
		self.multi_donnee = {}
		
	def multi_save(self,				#to save some data ... (step -1 always)
			tar = 'int',			#ad salvandum designatus
			file_name = 'multi.npsave'):	#where we will save it
		with open(self.address+file_name, 'wb') as f: pickle.dump(self.multi_donnee[tar], f) 
		f.close()
	
	def multi_save_cef(self,			#to save some data ... (step -1 sometimes)
			tar = 'int',			#ad salvandum designatus
			tar_event = '30feb00_2561',	#if necessary
			file_name = 'multi.cef',	#where we will save it (after the address I already have)
			mode = 'w',			#put "a" for adding, "w" for writing
			dec_zeros = None):		#number of decimal zeros for each variable - the default gives six decimal positions to every variable
		
		if tar_event != None : 
			tar_data = self.multi_donnee[tar][tar_event].donnee
		else :
			tar_data = self.multi_donnee[tar].donnee
		
		writecef.cefwrite(data=tar_data,filename=file_name,mode=mode,dec_zeros=dec_zeros)
		
	
	def multi_get_cef(self,						#gets raw data from .cef files (possible step 1)
			address = '/home/fadanelli/Desktop/data/',	#where do you keep your events? 
			event_names = '02Mar02_0331',			#which event do we refer to? (if 'ALL', it will take them all!) (ex:  'time_table_20150908_100000_20150908_113000')
			var_names = ['B','r','v'],			#the quantities' names
			sat_names = ['SC1', 'SC2', 'SC3', 'SC4'],	#the satellites' names
			aka = 'raw',					#how do you want me to call them?
			ref_frame = 'gse'):				#the initial reference frame - needed only for sid_names 
		self.address = address
		self.var_names = var_names
		self.sat_names = sat_names
		#self.ref_frame = ref_frame
		
		if event_names is 'ALL':
			self.event_names = []
			for event_name in os.listdir(self.address):
				total_path = os.path.join(self.address,event_name)
				if os.path.isdir(total_path) :
					self.event_names.append(event_name)
			print self.event_names
		else : 
			self.event_names = event_names
		
		
		
			## WARNING you must declare somehow your translator WARNING ##
		# actually, for a first try just set it manually - ex: self.translator = {'B_SC1':'obj3', 'B_SC2':'obj4', ... }
		with open(self.address+'translator.npsave', 'rb') as fp: trans = pickle.load(fp) 
		fp.close()
		self.translator = trans[ref_frame]
		#print self.translator
		
		
		self.multi_donnee[aka] = {}
		
		for event_name in self.event_names:
			self.multi_donnee['raw'][event_name] = {}
			for var_name in self.var_names:
				self.multi_donnee['raw'][event_name][var_name] = {}
				
				for s in self.sat_names:
					#print self.translator[var_name+'_'+s]+'.cef'
					self.multi_donnee['raw'][event_name][var_name][s] = mms.donnee(fichier=(self.address+event_name+'/'+self.translator[var_name+'_'+s]+'.cef'))
	
	def multi_get_npsave(self,					#restores data saved in *.npsaved  (other possible step 1)
			address = '/home/fadanelli/Desktop/data/',	#where do you keep your events? 
			#var_names = ['B','r','v'],			#the quantities' names
			sat_names = ['SC1', 'SC2', 'SC3', 'SC4'],	#the satellites' names
			aka = 'res',					#qui resuscitandum est, nomen
			ref_frame = 'gse',				#the initial reference frame - needed only for sid_names 
			file_name = 'multi.npsave'): 			#from which we take the data
		self.address = address
		#self.var_names = var_names
		self.sat_names = sat_names
		self.ref_frame = ref_frame
		
		self.multi_donnee[aka] = {}
		
		with open(self.address+file_name, 'rb') as fp: self.multi_donnee[aka] = pickle.load(fp) 
		fp.close()
	
	
	def multi_interpolate_raw(self,			#interpolates the raw data to create only one donnee object
			ref_s = 'SC1',			#reference SC
			ref_var = 'B',			#reference variable
			aka = 'int',			#name of the final interpolated quantity 
			ref_frame = 'gse',		#yes, only for sid_names you need it
			sid_names = True):		#decide whether you want also new variable names - Sid's choice ;)
		
		self.multi_donnee[aka] = {}
		
		for event_name in self.event_names:
			self.multi_donnee[aka][event_name] = mms.donnee(fichier='pippo',virgin=True)
			
			self.multi_donnee[aka][event_name].donnee['epoch'] = np.copy((self.multi_donnee['raw'][event_name][ref_var][ref_s]).donnee['epoch'])
			self.multi_donnee[aka][event_name]._varnames = np.append(self.multi_donnee[aka][event_name]._varnames, np.array( ['epoch'])) 
			AbscSize = len(self.multi_donnee[aka][event_name].donnee['epoch'])
		
			for var_name in self.var_names:
				for s in self.sat_names:
					old_var_names = sorted((set(self.multi_donnee['raw'][event_name][var_name][s].get_varnames()) - {'epoch', 'K001', 'K002'}))
					coord, common = string_and(list(old_var_names))
					print var_name, '\t', s, '\t', old_var_names 		#check
					
					if len(old_var_names) == 9 : 
						shape = (AbscSize,3,3)
						if sid_names: 	new_name = var_name+'_'+ref_frame+'_'+s #+'_E'+str(event_num).zfill(3)
						else :		new_name = common+'_'+s
					elif len(old_var_names) == 3 : 
						shape = (AbscSize,3)
						if sid_names: 	new_name = var_name+'_'+ref_frame+'_'+s #+'_E'+str(event_num).zfill(3)
						else :		new_name = common+'_'+s
					elif len(old_var_names) == 2 : #case "para vs. perp"
						shape = (AbscSize,2)
						if sid_names: 	new_name = var_name+'_'+s #+'_E'+str(event_num).zfill(3)
						else :		new_name = common+'_'+s
					elif len(old_var_names) == 1 : 
						shape = (AbscSize)
						if sid_names: 	new_name = var_name+'_'+s #+'_E'+str(event_num).zfill(3)
						else :		new_name = common+'_'+s
					else: print 'What kind of variable are you dealing with? A (AbscSize,3,3,3) tensor???? Come and modify the code, you moron ...'
					self.multi_donnee[aka][event_name]._varnames = np.append(self.multi_donnee[aka][event_name]._varnames, np.array([new_name]))
					
					self.multi_donnee[aka][event_name].donnee[new_name] = np.zeros([AbscSize,len(old_var_names)])
					for i, old_name in enumerate(old_var_names): 
						#print i, old_name, new_name #, '\n', self.multi_donnee['raw'][event_name][var_name][s].donnee['epoch']
						self.multi_donnee[aka][event_name].donnee[new_name][:,i] = time_interpolation( 
							x1=self.multi_donnee[aka][event_name].donnee['epoch'],
							x2=self.multi_donnee['raw'][event_name][var_name][s].donnee['epoch'],
							y2=self.multi_donnee['raw'][event_name][var_name][s].get_data(old_name))
					self.multi_donnee[aka][event_name].donnee[new_name] = np.reshape(self.multi_donnee[aka][event_name].donnee[new_name],shape)
	
	def multi_compare(self,			#compares more events, normalizing their time intervals
			 tar = 'int',		#the target quantity
			 aka = 'all'):		#name for the processed data
		
		if aka not in self.multi_donnee : self.multi_donnee[aka] = mms.donnee(fichier='pippo',virgin=True)
		
		
		max_datalen = 1
		for event_name in self.event_names:
			max_datalen = max(max_datalen,(self.multi_donnee[tar][event_name].donnee['epoch']).__len__())
		#print max_datalen
		
		self.multi_donnee[aka].donnee['epoch_like'] = np.arange(max_datalen)
		
		for event_num, event_name in enumerate(self.event_names):
			var_names = self.multi_donnee[tar][event_name]._varnames
			for var_name in (set(var_names) - {'epoch'}):
				old_datalen = (self.multi_donnee[tar][event_name].donnee['epoch']).__len__()
				#old_absc = np.multiply(np.arange(old_datalen), max_datalen/float(old_datalen))
				new_var_name = var_name+'_E'+str(event_num).zfill(3)
				#print 'E'+str(event_num).zfill(3), var_name, max_datalen, old_datalen, old_absc[-1], old_absc[-2], old_absc[-3]
				#print (self.multi_donnee[tar][event_name].donnee[var_name]).__len__()
				
				self.multi_donnee[aka].donnee[new_var_name] = time_interpolation_plus(
					x1=self.multi_donnee[aka].donnee['epoch_like'],
					x2=np.multiply(np.arange(old_datalen), max_datalen/float(old_datalen)),
					y2=self.multi_donnee[tar][event_name].get_data(var_name))
				self.multi_donnee[aka]._varnames = np.append(self.multi_donnee[aka]._varnames,np.array([new_var_name]))
	
	def multi_sums(self,		#sums values of corresponding arrays in different events 
			tar_vars, 	
			tar = 'int',
			aka = 'all'):
		
		if aka not in self.multi_donnee : self.multi_donnee[aka] = mms.donnee(fichier='pippo',virgin=True)
		
		for nom in tar_vars: 
			for ev, event_name in enumerate(self.event_names):
				
				if ev == 0 :
					self.multi_donnee[aka].donnee[nom] = np.copy(self.multi_donnee[tar][event_name].donnee[nom])
				else :
					self.multi_donnee[aka].donnee[nom] = np.add(self.multi_donnee[aka].donnee[nom], self.multi_donnee[tar][event_name].donnee[nom])
			self.multi_donnee[aka]._varnames = np.append(self.multi_donnee[aka]._varnames,np.array([nom]))
	
	
	def multi_merge(self,
			tar_vars, 
			tar = 'int',
			aka = 'all'):
		
		if aka not in self.multi_donnee : self.multi_donnee[aka] = mms.donnee(fichier='pippo',virgin=True)
		
		for nom in tar_vars: 
			for ev, event_name in enumerate(self.event_names):	
				if ev == 0 :
					self.multi_donnee[aka].donnee[nom] = np.copy(self.multi_donnee[tar][event_name].donnee[nom])
				else :
					self.multi_donnee[aka].donnee[nom] = np.concatenate((self.multi_donnee[aka].donnee[nom],self.multi_donnee[tar][event_name].donnee[nom]),axis=0)
			self.multi_donnee[aka]._varnames = np.append(self.multi_donnee[aka]._varnames,np.array([nom]))
		
	
	## fuctions for data separated in events - these are all pointed onto 'int' by default! 
	
	def multi_t_average(self,			#returns another donnee object, with the time average of a given donnee object (could be step 2)
			half_width, 			#half width of the local averaging interval
			step,				#how many points to jump forward between one average and the next
			weights = None,			#weights for the average window
			t1 = None,			#initial time
			t2 = None,			#end time
			tar_event = '02Mar02_0331',	#which event are we working on?
			tar = 'int',			#name of the donnee object to be averaged
			aka = 'tav'):			#name of the donnee object that will hold the averaged ... let's put that you have to make it yourself
		
		if aka not in self.multi_donnee : self.multi_donnee[aka] = {}
		
		self.multi_donnee[aka][tar_event] = mms.donnee(fichier='pippo',virgin=True)
		
		self.multi_donnee[aka][tar_event].donnee['epoch'] = self.multi_donnee[tar][tar_event].new_epoch(half_width=half_width,step=step,t1=t1,t2=t2)
		self.multi_donnee[aka][tar_event]._varnames = np.append(self.multi_donnee[aka][tar_event]._varnames, np.array( ['epoch'])) 
		
		for nom in (set(self.multi_donnee[tar][tar_event]._varnames) - {'epoch'}):
			self.multi_donnee[aka][tar_event].donnee[nom]=self.multi_donnee[tar][tar_event].new_field(var=nom,half_width=half_width,step=step,weights=weights,t1=t1,t2=t2)
			self.multi_donnee[aka][tar_event]._varnames = np.append(self.multi_donnee[aka][tar_event]._varnames,np.array([nom]))
	
	def multi_SC_average(self,			#finds the SC average of the quantity
			tar_vars ,			#target variables - ex: ['B_gse','E_gse'] in sid_names
			tar = 'int',			#target of the procedure
			tar_event = '02Mar02_0331',	#target event of the procedure
			label = 'SCa'):			#label for SC-averaged quantities
		
		for nom in tar_vars: 
			self.multi_donnee[tar][tar_event].donnee[nom+'_'+label] = np.zeros(np.shape(self.multi_donnee[tar][tar_event].donnee[nom+'_SC1'])) 
			for s in self.sat_names:
				self.multi_donnee[tar][tar_event].donnee[nom+'_'+label] += np.multiply(0.25, self.multi_donnee[tar][tar_event].donnee[nom+'_'+s])
			self.multi_donnee[tar][tar_event]._varnames = np.append(self.multi_donnee[tar][tar_event]._varnames,np.array([nom+'_'+label]))
			
	def multi_minimal_stat(self,			#minimal statistic instrument: average, median, variance of the selected variables
			tar_vars, 			#target variables
			tar = 'int',			#target of the procedure
			tar_event = '02Mar02_0331',	#target event of the procedure
			labels = ['ave','med','var']):	#label for SC-averaged quantities
	
		for nom in tar_vars:
			self.multi_donnee[tar][tar_event].donnee[nom+'_'+labels[0]] = np.average(self.multi_donnee[tar][tar_event].donnee[nom],axis=0)
			self.multi_donnee[tar][tar_event].donnee[nom+'_'+labels[1]] = np.median(self.multi_donnee[tar][tar_event].donnee[nom],axis=0)
			self.multi_donnee[tar][tar_event].donnee[nom+'_'+labels[2]] = np.var(self.multi_donnee[tar][tar_event].donnee[nom],axis=0)
			self.multi_donnee[tar][tar_event]._varnames = np.append(self.multi_donnee[tar][tar_event]._varnames,np.array([nom+'_'+labels[0]]))
			self.multi_donnee[tar][tar_event]._varnames = np.append(self.multi_donnee[tar][tar_event]._varnames,np.array([nom+'_'+labels[1]]))
			self.multi_donnee[tar][tar_event]._varnames = np.append(self.multi_donnee[tar][tar_event]._varnames,np.array([nom+'_'+labels[2]]))
	
	
	def multi_v_normalize(self, 			#calculates normalized version of vector quantities
			tar_vars,			#target (vector!) variables - ex: ['B_gse','B_gsm'] in sid_names
			tar = 'int',			#target of the procedure
			tar_event = '02Mar02_0331',	#target event of the procedure
			label_norm = 'n',		#label for the (scalar!) norms of the initia (vector!) fields
			label_dir = 'e'):		#label for the normalized (vector!) fields
		
		old_var_names = np.copy(self.multi_donnee[tar][tar_event]._varnames)
		for nom in tar_vars:
			for noms in old_var_names:
				if len(nom) == len(noms) - 4 :
					#print nom, noms
					ww, com = string_and([nom+'****', noms])
					if com == nom : 
						#print nom, noms
						self.multi_donnee[tar][tar_event].donnee[label_norm+noms], self.multi_donnee[tar][tar_event].donnee[label_dir+noms] = normalize(
							self.multi_donnee[tar][tar_event].donnee[noms]) 
						self.multi_donnee[tar][tar_event]._varnames = np.append(self.multi_donnee[tar][tar_event]._varnames,np.array([label_norm+noms, label_dir+noms]))
	
	def multi_SC_difference(self, 			#gives all differences
			tar_vars,			#target variables - ex: ['B_gse','B_gsm'] in sid_names
			tar = 'int',			#target of the procedure
			tar_event = '02Mar02_0331',	#target event of the procedure
			redundant = False,		#calculate also the redundant terms (1D2 & 2D1 are redundant ...)
			label = 'D'):			#label for the difference-among-spacecraft fields
		
		
		for s in range(1,len(self.sat_names)+1):
			if redundant : sss = 1
			else : sss = s+1
			for ss in range(sss,len(self.sat_names)+1):
				for nom in tar_vars:
					self.multi_donnee[tar][tar_event].donnee[nom+'_'+str(s)+label+str(ss)] = np.add(
						self.multi_donnee[tar][tar_event].donnee[nom+'_SC'+str(s)],
						-self.multi_donnee[tar][tar_event].donnee[nom+'_SC'+str(ss)])
					self.multi_donnee[tar][tar_event]._varnames = np.append(self.multi_donnee[tar][tar_event]._varnames,np.array([nom+'_'+str(s)+label+str(ss)]))
	
	def multi_gradient(self,			#calculates the gradient of some given variable 
			tar_vars,			#target variables - ex: ['B_gse', 'Pe_gse'] in sid_names
			pos_var = 'r_gse', 		#position variables - ex: 'r_gse' in sid_names
			tar = 'int',			#target of the procedure
			tar_event = '02Mar02_0331',	#target event of the procedure
			ref_frame = 'gse',		#reference frame ...
			label = 'd'):			#label for the gradient quantities
		
		absc_size = len(self.multi_donnee[tar][tar_event].donnee['epoch'])
		sat_number = len(self.sat_names)
		
		#bring together all spatial differences in the useful fields
		self.multi_SC_difference(tar_vars=tar_vars,tar=tar,tar_event=tar_event)
		
		#calculate vecs of spatial diff, then inverse of the vol_tens (only if they have not been calculated yet)
		if 'vol'+pos_var not in self.multi_donnee[tar][tar_event]._varnames : 
			#print 'your grad is currently processing', pos_var
			self.multi_SC_difference(tar_vars=[pos_var],tar=tar,tar_event=tar_event)
			
			self.multi_donnee[tar][tar_event].donnee['vol'+pos_var] = np.zeros([absc_size,3,3], dtype=float)
			for k in range(absc_size):
				for s in range(1,sat_number+1):
					for ss in range(s+1,sat_number+1):
						self.multi_donnee[tar][tar_event].donnee['vol'+pos_var][k,:,:] += np.tensordot(
							self.multi_donnee[tar][tar_event].donnee[pos_var+'_'+str(s)+'D'+str(ss)][k,:],
							self.multi_donnee[tar][tar_event].donnee[pos_var+'_'+str(s)+'D'+str(ss)][k,:],axes=0)
				self.multi_donnee[tar][tar_event].donnee['vol'+pos_var][k,:,:] = np.linalg.inv(
					self.multi_donnee[tar][tar_event].donnee['vol'+pos_var][k,:,:])
			self.multi_donnee[tar][tar_event]._varnames = np.append(self.multi_donnee[tar][tar_event]._varnames,np.array(['vol'+pos_var]))
		
		#for k in range(5):
		#	print self.multi_donnee[tar][tar_event].donnee['vol'+pos_var][k,:,:]
		
		#calculate vector to multiply vol_tens with, do it!
		for nom in tar_vars :
			vec, shape = my_reshape(self.multi_donnee[tar][tar_event].donnee[nom+'_SC1'])
			tens = np.zeros([absc_size,3,np.shape(vec)[1]], dtype=float)
			print np.shape(vec), np.shape(self.multi_donnee[tar][tar_event].donnee[pos_var+'_1D2']), np.shape(tens)
			shape = np.insert(np.array(shape), 1, 3)
			
			
			for s in range(1,sat_number+1):
				for ss in range(s+1,sat_number+1):
					vec, ww = my_reshape(self.multi_donnee[tar][tar_event].donnee[nom+'_'+str(s)+'D'+str(ss)])
					#if s==1 and ss==2 : 
						#print vec[0,:]
						#print self.multi_donnee[tar][tar_event].donnee[nom+'_1D2'][0,:]
					for k in range(absc_size):
						tens[k,:,:] += np.tensordot(self.multi_donnee[tar][tar_event].donnee[pos_var+'_'+str(s)+'D'+str(ss)][k,:],vec[k,:],axes=0)
			for k in range(absc_size):
				tens[k,:,:] = np.dot( self.multi_donnee[tar][tar_event].donnee['vol'+pos_var][k,:,:],tens[k,:,:])
			
			
			nom = string_replace(nom,ref_frame)
			
			self.multi_donnee[tar][tar_event].donnee[label+nom] = np.reshape(tens, shape) 
			self.multi_donnee[tar][tar_event]._varnames = np.append(self.multi_donnee[tar][tar_event]._varnames,np.array([label+nom]))
	
	def multi_gradient_wow(self,			#calculates the gradient of some given variable, but is much wow (uses reciprocal vectors)
			tar_vars,			#target variables - ex: ['B_gse', 'Pe_gse'] in sid_names
			pos_var = 'r_gse', 		#position variables - ex: 'r_gse' in sid_names
			tar = 'int',			#target of the procedure
			tar_event = '02Mar02_0331',	#target event of the procedure
			ref_frame = 'gse',		#reference frame ...
			label = 'g'):			#label for the gradient quantities
		
		absc_size = len(self.multi_donnee[tar][tar_event].donnee['epoch'])
		sat_number = len(self.sat_names)
		
		#calculate vecs of spatial diff, then inverse of the vol_tens (only if they have not been calculated yet)
		if 'inv'+pos_var not in self.multi_donnee[tar][tar_event]._varnames : 
			#print 'your grad is currently processing', pos_var
			self.multi_SC_difference(tar_vars=[pos_var],tar=tar,tar_event=tar_event,redundant=True)
			self.multi_donnee[tar][tar_event].donnee['checkvol'+pos_var] = np.zeros([absc_size,3,3])
			
			sat_perm = quasi_repeat(range(1,sat_number+1),0,sat_number-1)
			sat_perm = sat_perm.astype(int)
			#print sat_perm
			
			for s in range(1,sat_number+1):
				self.multi_donnee[tar][tar_event].donnee['inv'+pos_var+'_SC'+str(s)] = np.zeros([absc_size,3], dtype=float)
				#couple1 = np.sort(np.array([str(sat_perm[s]),str(sat_perm[s+1])]))
				#couple2 = np.sort(np.array([str(sat_perm[s]),str(sat_perm[s+2])]))
				#couple3 = np.sort(np.array([str(sat_perm[s-1]),str(sat_perm[s])]))
				#print couple1, couple2, couple3
				for k in range(absc_size):
					self.multi_donnee[tar][tar_event].donnee['inv'+pos_var+'_SC'+str(s)][k,:] = np.cross(
						self.multi_donnee[tar][tar_event].donnee[pos_var+'_'+str(sat_perm[s])+'D'+str(sat_perm[s+1])][k,:],
						self.multi_donnee[tar][tar_event].donnee[pos_var+'_'+str(sat_perm[s])+'D'+str(sat_perm[s+2])][k,:])
					self.multi_donnee[tar][tar_event].donnee['inv'+pos_var+'_SC'+str(s)][k,:] = np.divide(
						self.multi_donnee[tar][tar_event].donnee['inv'+pos_var+'_SC'+str(s)][k,:],np.dot(
						self.multi_donnee[tar][tar_event].donnee[pos_var+'_'+str(sat_perm[s-1])+'D'+str(sat_perm[s])][k,:],
						self.multi_donnee[tar][tar_event].donnee['inv'+pos_var+'_SC'+str(s)][k,:]))
					
					self.multi_donnee[tar][tar_event].donnee['checkvol'+pos_var][k,:,:] += np.tensordot( #should be equal to ['vol'+pos_var]
						self.multi_donnee[tar][tar_event].donnee['inv'+pos_var+'_SC'+str(s)][k,:],
						self.multi_donnee[tar][tar_event].donnee['inv'+pos_var+'_SC'+str(s)][k,:],axes=0) / 4.
					
				self.multi_donnee[tar][tar_event]._varnames = np.append(self.multi_donnee[tar][tar_event]._varnames,np.array(['inv'+pos_var+'_SC'+str(s)]))
				
				  
		
		#calculate vector to multiply vol_tens with, do it!
		for nom in tar_vars :
			vec, shape = my_reshape(self.multi_donnee[tar][tar_event].donnee[nom+'_SC1'])
			tens = np.zeros([absc_size,3,np.shape(vec)[1]], dtype=float)
			#print np.shape(vec), np.shape(self.multi_donnee[tar][tar_event].donnee[pos_var+'_1D2']), np.shape(tens)
			shape = np.insert(np.array(shape), 1, 3)
			
			
			for s in range(1,sat_number+1):
				vec, ww = my_reshape(self.multi_donnee[tar][tar_event].donnee[nom+'_SC'+str(s)])
				for k in range(absc_size):
					tens[k,:,:] += np.tensordot(self.multi_donnee[tar][tar_event].donnee['inv'+pos_var+'_SC'+str(s)][k,:],vec[k,:],axes=0)
			
			nom = string_replace(nom,ref_frame)
			
			self.multi_donnee[tar][tar_event].donnee[label+nom] = np.reshape(tens, shape) 
			self.multi_donnee[tar][tar_event]._varnames = np.append(self.multi_donnee[tar][tar_event]._varnames,np.array([label+nom]))
		
	
	
	def multi_v_project(self,			#projects the vector fields with some given matrix
			trans_mat,			#transposition matrix, (3,3) object or (absc_size,3,3) object
			tar_vars, 			#target variables of the procedure - ex: ['B_gse','ue_lmn'] 
			tar = 'int',			#who shall we transpose?
			tar_event = '02mar02_0331',	#target event
			new_ref = 'lmn'):			#name of the transposed quantity
		
		
		
		absc_size = len(self.multi_donnee[tar][tar_event].donnee['epoch'])
		if np.shape(trans_mat) == (3,3) : trans_mat = np.tile(trans_mat,(absc_size,1,1))
		
		for nom in tar_vars :
			new_nom = string_replace(nom,new_ref)
			self.multi_donnee[tar][tar_event].donnee[new_nom] = project_v_field(
				old_field = self.multi_donnee[tar][tar_event].donnee[nom], 
				trans_mat = trans_mat)
			self.multi_donnee[tar][tar_event]._varnames = np.append(self.multi_donnee[tar][tar_event]._varnames,np.array([new_nom]))
		

	def multi_vv_project(self,			#projects the tensor fields with some given matrix
			trans_mat,			#transposition matrix, (3,3) object or (absc_size,3,3) object
			tar_vars, 			#target variables of the procedure - ex: ['B_gse','ue_lmn'] 
			tar = 'int',			#who shall we transpose?
			tar_event = '02mar02_0331',	#target event
			new_ref = 'lmn'):		#name of the transposed quantity
		
		
		
		absc_size = len(self.multi_donnee[tar][tar_event].donnee['epoch'])
		if np.shape(trans_mat) == (3,3) : trans_mat = np.tile(trans_mat,(absc_size,1,1))
		
		for nom in tar_vars :
			new_nom = string_replace(nom,new_ref)
			self.multi_donnee[tar][tar_event].donnee[new_nom] = project_vv_field(
				old_field = self.multi_donnee[tar][tar_event].donnee[nom], 
				trans_mat = trans_mat)
			self.multi_donnee[tar][tar_event]._varnames = np.append(self.multi_donnee[tar][tar_event]._varnames,np.array([new_nom]))

	def multi_t_integrate(self,			#performs time integration, only on scalars alas!
			tar_var,			#target variable (ex: ['B_gse', 'cB_mva'])
			ref_t,				#reference time, at which we take the zero values for each of the variables (ex: [datetime(1,1,1), datetime(1,2,1)])
			tar = 'int',			#target class
			tar_event = '02mar02_0331',	#target event
			label = 'i') : 			#label denoting the integrated quantities
		
		new_nom = label+tar_var
		new_array, shape = my_reshape(self.multi_donnee[tar][tar_event].donnee[tar_var])
		new_ref_t, shape_t = my_reshape(ref_t)
		
		if shape[1] != shape_t[0] : print 'mazzescemo chessei'
		
		new_array = np.cumsum(new_array, axis=0)
		for i in range(shape[1]) :
			ref_t = abs(self.multi_donnee[tar][tar_event].donnee['epoch'] - new_ref_t[i]).argmin()
			#print ref_t
			new_array[:,i] -= new_array[ref_t,i]
		
		self.multi_donnee[tar][tar_event].donnee[new_nom] = new_array
		self.multi_donnee[tar][tar_event]._varnames = np.append(self.multi_donnee[tar][tar_event]._varnames,np.array([new_nom]))
		
	#def multi_mva(self,				#performs mva
			#tar_vars,			#target variables (ex: ['B_gse', 'cB_mva'])
			#t1 = None,			#initial time
			#t2 = None, 			#end time
			#tar = 'int',			#target class
			#tar_event = '02Mar02_0331',	#target event
			#new_ref = 'mva') : 		#label denoting the mva results
		
		#for nom in tar_vars :
			#new_nom = string_replace(nom,new_ref)
			#self.multi_donnee[tar][tar_event].donnee[new_nom] = self.new_field(var=nom,t1=t1,t2=t2)
			#self.multi_donnee[tar][tar_event]._varnames = np.append(self.multi_donnee[tar][tar_event]._varnames,np.array([new_nom]))
	
	def multi_calc_fou(self,			#creates a new class with fourier-transformed data 
			tar_vars,			#on which variables?  
			absolute = True,		#sall I kill the phase info?
			aliasing = False,		#shall I plot also above the Nyquist frequency?
			tar = 'int',			#target class
			tar_event = '02mar02_0331',	#target event
			aka = 'fou'):			#name of the new class
		
		if aka not in self.multi_donnee : self.multi_donnee[aka] = {}
		if tar_event not in self.multi_donnee[aka] : self.multi_donnee[aka][tar_event] = mms.donnee(fichier='pippo',virgin=True)
		
		
		absc_size_old = len(self.multi_donnee[tar][tar_event].donnee['epoch'])
		time_delta_old = tofloat(self.multi_donnee[tar][tar_event].donnee['epoch'][-1]) - tofloat(self.multi_donnee[tar][tar_event].donnee['epoch'][0])
		self.multi_donnee[aka][tar_event].donnee['freqs'] = np.linspace(0,absc_size_old*2*np.pi/time_delta_old,absc_size_old)
		#self.multi_donnee[tar][event].timestep()
		
		for nom in tar_vars:
			self.multi_donnee[aka][tar_event].donnee[nom] = np.fft.rfft(self.multi_donnee[tar][tar_event].donnee[nom],axis=0)
			if absolute : self.multi_donnee[aka][tar_event].donnee[nom] = np.absolute(self.multi_donnee[aka][tar_event].donnee[nom])
			if not aliasing : 
				self.multi_donnee[aka][tar_event].donnee[nom] = self.multi_donnee[aka][tar_event].donnee[nom][:absc_size_old//2]
				self.multi_donnee[aka][tar_event].donnee['freqs'] = self.multi_donnee[aka][tar_event].donnee['freqs'][:absc_size_old//2]

	
	#def multi_pass_low(self,
			#t1 = None,
			#t2 = None,
			#aka = 'low'):
		 
		
		
	#def multi_t_derive(self,		#effectuates the time derivative of each
			#tar = 'int',			#who shall we time-derive?
			#aka = 'tdr'):		#name of the time derivative quantity
		
		#self.multi_donnee[aka] = {}
		#for s in self.sat_names:
			#self.multi_donnee[aka][s] = np.gradient(self.multi_donnee[tar][s], axis=0)/self.timestep
	
	def multi_sph_angles(self,			#calculates 
			tar_vars, 			#target variables of the procedure - ex: ['B_gse','ue_lmn'] 
			degrees = False,		#shall I give your result in degrees?
			half = False,			#shall I revert all angles on the z-positive hemisphere?
			tar = 'int',			#who shall we transpose?
			tar_event = '02mar02_0331',	#target event
			label = 'a'):			#name of the transposed quantity
		
		absc_size = len(self.multi_donnee[tar][tar_event].donnee['epoch'])
		
		
		for nom in tar_vars :
			new_nom = label+nom
			
			x_vec = self.multi_donnee[tar][tar_event].donnee[nom][:,0]
			y_vec = self.multi_donnee[tar][tar_event].donnee[nom][:,1]
			z_vec = self.multi_donnee[tar][tar_event].donnee[nom][:,2]
			if half : 
				z_vec = np.absolute(z_vec)
				invert = z_vec != self.multi_donnee[tar][tar_event].donnee[nom][:,2]
				x_vec[invert] = - x_vec[invert]
				y_vec[invert] = - y_vec[invert]
			
			self.multi_donnee[tar][tar_event].donnee[new_nom] = np.zeros([absc_size,2])
			self.multi_donnee[tar][tar_event].donnee[new_nom][:,0] = np.arccos(z_vec)
			#self.multi_donnee[tar][tar_event].donnee[new_nom][:,1] = np.arccos(np.divide(x_vec,np.sin(self.multi_donnee[tar][tar_event].donnee[new_nom][:,0])))
			self.multi_donnee[tar][tar_event].donnee[new_nom][:,1] = np.arctan(np.divide(x_vec,y_vec))
			#minus = y_vec < 0.0
			#self.multi_donnee[tar][tar_event].donnee[new_nom][minus,1] = -self.multi_donnee[tar][tar_event].donnee[new_nom][minus,1]
			
			
			if degrees : self.multi_donnee[tar][tar_event].donnee[new_nom] = np.rad2deg(self.multi_donnee[tar][tar_event].donnee[new_nom]) 
			
			self.multi_donnee[tar][tar_event]._varnames = np.append(self.multi_donnee[tar][tar_event]._varnames,np.array([new_nom]))
	
	
	def multi_plot(self,						#provides a quick plot of the quantity
			tar_vars = ['B_gse_SC1', 'B_gse_SC2'],		#which variables should we plot?
			tar_names = ['noname_1', 'noname_2'],		#with which names?
			coor_names = [[False,False],[False]],		#names of the coordinates
			tar = 'int',					#who shall we plot?
			tar_event = '02mar02_0331',			#which event?
			t1 = None,					#begin time
			t2 = None,					#end time
			plot_title = '',				#addition to the plot's title
			fig_title = '',					#addition to the figure's title
			grid = True,					#grid?
			show = True,					#show? 
			fig = True,					#figure?
			yscale = 'linear'):				#set 'linear' or 'log' ... I don't need to explain
		
		
		x_ax = self.multi_donnee[tar][tar_event].new_epoch(t1=t1,t2=t2)
		
		var_number = len(tar_vars)
		if var_number != len(tar_names) : tar_names = tar_vars
		if fig : f = plt.figure(figsize=(10,var_number*2))
		
		for v, var in enumerate(tar_vars) : 
			print 'plotting', var
			wow_plot = plt.subplot(var_number,1,v+1)
			
			y_ax, ww = my_reshape(self.multi_donnee[tar][tar_event].new_field(var=var,t1=t1,t2=t2))
			var_dims = np.shape(y_ax)[1] 
			
			
			
			lineObjects = wow_plot.plot_date(x_ax, y_ax, '-' )
			
			if all(coor_names[v]) != False : plt.legend(iter(lineObjects), coor_names[v])
			elif var_dims == 3 : plt.legend(iter(lineObjects), ['x','y','z'])
			elif var_dims == 9 : plt.legend(iter(lineObjects), ['xx','xy','xz','yx','yy','yz','zx','zy','zz'])
			elif var_dims != 1 : print 'boia ma che plotti??'
			
			if v+1 != var_number : plt.setp(wow_plot.get_xticklabels(), visible=False)
			plt.ylabel(tar_names[v])
			
			if grid : plt.grid(True)
		
		plt.title(plot_title)
		plt.tight_layout()
		if show : plt.show()
		
		if fig : f.savefig(self.address+tar_event+'/images/'+fig_title+'.pdf')
	
	def multi_add_error (self,
		      tar,
		      tar_event,
		      tar_var,
		      err_val,
		      label='#') : 
		
		absc_size = len(self.multi_donnee[tar][tar_event].donnee['epoch'])
		self.multi_donnee[tar][tar_event].donnee[label+tar_var] = self.multi_donnee[tar][tar_event].donnee[tar_var] + 2 * err_val * ( np.random.rand(absc_size,3) - 0.5)
	


##FUNCTIONS
#def translator(ref_frame):
	#infos = {}
	#infos['CLUSTER'] = {}
	#infos['CLUSTER']['B'] = ['B_vec_xyz_'+ref_frame+'_x','B_vec_xyz_'+ref_frame+'_y','B_vec_xyz_'+ref_frame+'_z']# NB :['Bx_gse_nT','By_gse_nT','Bz_gse_nT'] 
	#infos['CLUSTER']['pos'] = ['sc_r_xyz_'+ref_frame+'_x','sc_r_xyz_'+ref_frame+'_y','sc_r_xyz_'+ref_frame+'_z']
	#infos['CLUSTER']['vel'] = ['sc_v_xyz_'+ref_frame+'_x','sc_v_xyz_'+ref_frame+'_y','sc_v_xyz_'+ref_frame+'_z']
	#infos['MMS'] = {}
	#infos['MMS']['B'] = ['fgm_bx_'+ref_frame+'_brst_l2','fgm_by_'+ref_frame+'_brst_l2','fgm_bz_'+ref_frame+'_brst_l2']
	#infos['MMS']['pos'] = ['mec_rx_'+ref_frame,'mec_ry_'+ref_frame,'mec_rz_'+ref_frame]
	#infos['MMS']['vel'] = ['mec_vx_'+ref_frame,'mec_vy_'+ref_frame,'mec_vz_'+ref_frame]
	#return infos

def string_and(st_old): 
	number = st_old.__len__()
	length = len(st_old[0])
	record = np.zeros(length, dtype=bool)
	for i in range(0,length):
		cfr = ''
		for j in range(0,number):
			cfr = cfr+st_old[j][i]
		record[i] = (cfr != ''.join([st_old[0][i]*number]))
		
	st_new = ['']*number
	for j in range(0,number):
		st_new[j] = ''.join(np.array(list(st_old[j]))[record])
		
	drocer = record == False
	st_common = ''.join(np.array(list(st_old[0]))[drocer])
	
	return st_new, st_common

def normalize(list_elements):
	vector = np.array(list_elements)
	norm = np.linalg.norm(vector, axis=1) #achtung that this works only when ...
	vector = vector / np.expand_dims(norm, 1)
	return norm, vector


def time_interpolation(x1, x2, y2) :
	"""
	Time interpolation for 1D data :
	x1 = reference abscisse (in datetime objects)
	x2, y2 = abscisse and ordinate of data to interpolate
	return y3 = y2 on x1
	"""
	def tofloat(d) :
		ref = datetime(2001,1,1) #Timedelta Calculé à partir de 2001!!
		return (d-ref).total_seconds()
	tofloat = np.vectorize(tofloat)
	return np.interp(tofloat(x1),tofloat(x2) ,y2)

def time_interpolation_plus(x1,x2,y2):
	"""
	Time interpolation for multiD data :
	x1 = reference abscisse (in datetime objects)
	x2, y2 = abscisse and ordinate of data to interpolate
	return y3 = y2 on x1
	"""
	#print x1.dtype, x2.dtype, y2.dtype
	
	if x1.dtype == datetime : x1 = tofloat(x1)
	if x2.dtype == datetime : x2 = tofloat(x2)
	
	y2, shape = my_reshape(y2)
	
	for i in range(y2.shape[1]) :
		y2[:,i] = np.interp(x1,x2,y2[:,i]) 
	
	return np.reshape(y2,shape)
      
def tofloat(d) :
	ref = datetime(2001,1,1) #Timedelta Calculé à partir de 2001!!
	return (d-ref).total_seconds()
tofloat = np.vectorize(tofloat)

def my_reshape(vector):
	a = np.shape(vector)
	
	if a == (a[0],) : 
		vector = np.reshape(vector, (a[0],1))
		a = np.shape(vector)
		return vector, a
	
	else :
		b = 1
		for i in range(1,len(a)): 
			b = b*a[i]
		return np.reshape(vector,(a[0],b)), a
 
 
def project_v_field(old_field, trans_mat):
	length = np.shape(old_field)[0]
	#print np.shape(old_field), np.shape(trans_mat)
	
	new_field = np.zeros([length,3])
	for i in range(0,length):
		new_field[i,:] = np.dot(trans_mat[i,:,:],old_field[i,:])
	return new_field

def project_vv_field(old_field, trans_mat):
	length = np.shape(old_field)[0]
	#print np.shape(old_field), np.shape(trans_mat)
	
	new_field = np.zeros([length,3,3])
	for i in range(0,length):
		new_field[i,:] = np.dot(np.transpose(trans_mat[i,:,:]),old_field[i,:,:])
		new_field[i,:] = np.dot(new_field[i,:,:],trans_mat[i,:,:])
	return new_field


def string_replace(st_old,rep): 	#achtung: works properly only with sid_names, where vectors have the ref frame after the first '_'  
	splitted = st.split(st_old,sep='_')
	if len(splitted) == 1 : 
		st_new = splitted[0]+'_'+rep
	else : 
		splitted[1] = rep
		st_new = '_'.join(splitted)
	return st_new

def quasi_repeat(vec,anti,post):	#the vector, the numbers of positions it has to be augmented before and after ...
	vec = np.concatenate((vec[-anti-1:-1],vec,vec[0:post]))
	return vec


#class field(np.ndarray) :
  
	#def __init__(self, array):
	#self = array
	#self.header = {}
	#for i in self.shape[1] : self.header[i] = ''
	
	
	#def set_header(self, col, header) :
		#self.header[col] = header
		



#class list_data(object) :
  
	#def __init__(self) :
		#self.chemin = '/data'
		
		#self.tt_dirs = []
		#for tt_rep in sorted(os.walk(self.chemin).next()[1]) : self.tt_dirs.append(tt_rep)
		
		
		
		#for tt_rep in glob.glob(chemin , )
		#for fichier in glob.glob(os.path.join(self.repertoire['default'].get(), self.listedir.selection_get(), '*.cef')):

'''
